#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysymdef.h>

#include <GL/gl.h>
#include <GL/glx.h>

#include <locale.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>

struct window {
    Display *display;
    Window window;
    Atom deleteWin;
};

static struct window _main_win = {NULL, 0, 0};

typedef GLXContext (*glXCreateContextAttribsARBProc) (Display *, GLXFBConfig, GLXContext, Bool, const int*) ;

static int isExtensionSupported(const char *extList, const char *extension) {
	return strstr(extList, extension) != 0;
}

struct window *create_window() {
    Screen *screen;
    int screen_id = -1;

    XInitThreads();
    setlocale(LC_ALL, "");

    _main_win.display = XOpenDisplay(NULL);
    if (!_main_win.display) {
        exit(1);
    }

    screen = DefaultScreenOfDisplay(_main_win.display);
    (void)screen;
    screen_id = DefaultScreen(_main_win.display);

    GLint major_GLX, minor_GLX = 0;
    glXQueryVersion(_main_win.display, &major_GLX, &minor_GLX);

    GLint glx_attrs[] = {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        GLX_DOUBLEBUFFER, True,
        None
    };

    int fbcount;
    GLXFBConfig *fbc = glXChooseFBConfig(_main_win.display, screen_id, glx_attrs, &fbcount);

    int best_fbc = -1,
        worst_fbc = -1,
        best_num_samp = -1,
        worst_num_samp = 999;

    for(int i = 0; i < fbcount; ++i) {
        XVisualInfo *vi = glXGetVisualFromFBConfig(_main_win.display, fbc[i]);
        if(vi) {
            int samp_buf, samples;
            glXGetFBConfigAttrib(_main_win.display, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf);
            glXGetFBConfigAttrib(_main_win.display, fbc[i], GLX_SAMPLES, &samples);

            if(best_fbc < 0 || (samp_buf && samples > best_num_samp)){
                best_fbc = i;
                best_num_samp = samples;
            }

            if(worst_fbc < 0 || !samp_buf || samples < worst_num_samp){
                worst_fbc = i;
                worst_num_samp = samples;
            }
        }
        XFree(vi);
    }
    GLXFBConfig best_fbc_conf = fbc[best_fbc];
    XFree(fbc);

    XVisualInfo *visual = glXGetVisualFromFBConfig(_main_win.display, best_fbc_conf);

    XSetWindowAttributes windowAttrs;
    windowAttrs.border_pixel = BlackPixel(_main_win.display, screen_id);
    windowAttrs.background_pixel = WhitePixel(_main_win.display, screen_id);
    windowAttrs.override_redirect = True;
    windowAttrs.colormap = XCreateColormap(_main_win.display, RootWindow(_main_win.display, screen_id), visual->visual, AllocNone);
    windowAttrs.event_mask = ExposureMask;
    _main_win.window = XCreateWindow(_main_win.display, RootWindow(_main_win.display, screen_id), 0, 0, 800, 600, 0, visual->depth, InputOutput, visual->visual, CWBackPixel | CWColormap | CWBorderPixel | CWEventMask, &windowAttrs);

    Atom atom_wm_delete_window = XInternAtom(_main_win.display, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(_main_win.display, _main_win.window, &atom_wm_delete_window, 1);
    _main_win.deleteWin = atom_wm_delete_window;

    glXCreateContextAttribsARBProc glx_create_attribARB = NULL;
    glx_create_attribARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB( (const GLubyte *) "glXCreateContextAttribsARB" );

    int context_attribs[] = {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
        GLX_CONTEXT_MINOR_VERSION_ARB, 2,
        GLX_CONTEXT_FLAGS_ARB, GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
        None
    };

    GLXContext context = 0;
    const char *glxExts = glXQueryExtensionsString(_main_win.display, screen_id);
    if(!isExtensionSupported(glxExts, "GLX_ARB_create_context")){
        context = glXCreateNewContext(_main_win.display, best_fbc_conf, GLX_RGBA_TYPE, 0, True);
    }else {
        context = glx_create_attribARB(_main_win.display, best_fbc_conf, 0, True, context_attribs);
    }

    XSync(_main_win.display, False);

    glXMakeCurrent(_main_win.display, _main_win.window, context);

    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glViewport(0, 0, 800, 600);

    XClearWindow(_main_win.display, _main_win.window);
    XMapRaised(_main_win.display, _main_win.window);

    return &_main_win;
}

static double get_miliseconds() {
	struct timeval s_tTimeVal;
	gettimeofday(&s_tTimeVal, NULL);
	double time = s_tTimeVal.tv_sec * 1000.0; // sec to ms
	time += s_tTimeVal.tv_usec / 1000.0; // us to ms
	return time;
}

int update(float delta) {
    (void) delta;
    return 0;
}

void render(void) {
    glClear(GL_COLOR_BUFFER_BIT);
}

void resize(int w, int h) {
    glViewport(0, 0, w, h);
}

int run_loop(struct window *win, double *time)
{
    if(*time == 0) {
        *time = get_miliseconds();
    }
    XEvent ev;
    if(XPending(win->display) > 0) {
        XNextEvent(win->display, &ev);
        if(ev.type == Expose) {
            XWindowAttributes attrs;
            XGetWindowAttributes(win->display, win->window, &attrs);
            resize(attrs.width, attrs.height);
        }
        if(ev.type == ClientMessage) {
            if(ev.xclient.data.l[0] == (long) win->deleteWin){
                return 1;
            }
        } else if(ev.type == DestroyNotify) {
            return 1;
        }
    }

    double now = get_miliseconds();
    double delta = (now - (*time)) * 0.001;
    *time = now;

    if(update(delta)) {
        return 1;
    }

    render();

    glXSwapBuffers(win->display, win->window);
    return 0;
}
